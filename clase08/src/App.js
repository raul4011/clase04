import PublicRoutes from './Routes/PublicRoutes';
import PrivateRoutes from './Routes/PrivateRoutes';
import './App.css';
import {AuthProvider} from "./context/Auth";

function App() {
  return (
    <>
      <AuthProvider>
        <PublicRoutes />
        <PrivateRoutes />
      </AuthProvider>
    </>
  );
}

export default App;
