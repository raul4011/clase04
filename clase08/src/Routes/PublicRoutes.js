import {BrowserRouter as Router, Route} from "react-router-dom";
import Login from "../Pages/Login";
import Dashboard from "../Pages/Dashboard";

const PublicRoutes = () => {
    return ( 
        <>
        <Router>
            <Route path="/Login" exact component={Login} />
            <Route path="/dashboard" exact component={Dashboard} />
        </Router>
        </>
     );
}
 
export default PublicRoutes;