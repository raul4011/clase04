import {BrowserRouter as Router, Route} from "react-router-dom";
import Dashboard from "../Pages/Dashboard";


const PrivateRoutes = () => {
    return ( 
        <>
        <Router>
            <Route path="/dashboard" exact component={Dashboard} />
           
        </Router>
        </>
     );
}
 
export default PrivateRoutes;