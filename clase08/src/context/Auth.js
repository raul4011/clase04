import React, {useState, createContent} from "react";

export const AuthContext = createContent({
    auth: null,
    authenticate: () => {},
    logout: () => {},
});

const {Provider} = AuthContext;

export const AuthProvider = ({children}) => {
    
    const [auth, setAuth] = useState(null);
    const authenticate = (token) => {
        setAuth(token);
    }
    const logout = () => {
        setAuth(null);
    };

    return <Provider value={{auth,authenticate,logout}}>{children}</Provider>;
}