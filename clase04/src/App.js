import {useState, useEffect} from "react";
import './App.css';

function App() {

  const [counter, setCounter] = useState(0);

  useEffect(() => {
    console.log("montage")
  },[]);

  const handleClick = () => {
    console.log("sumando")
    setCounter(counter + 1);
  }

  return (
  <>
    <h1>APP</h1>
    <h2>Contador: {counter}</h2>
    <button type={"button"} onClick={handleClick}>Boton</button>
  </>
  );
}

export default App;
