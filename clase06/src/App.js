//BrowserRouter --> nos da el historial de navegacion del cliente
import {BrowserRouter as Router, Route, Redirect} from "react-router-dom";
import Login from "./components/Pages/Login";
import Register from "./components/Pages/Register";
import Home from "./components/Pages/Home";
import Nav from "./components/Common/Nav";


function App() {
  return (
   <>
    <Router>
      <Nav />
      <Route path="/" exact component={Home} />
      <Route path="/login" component={Login} />
      <Route path="/register" component={Register} />
      <Redirect to="/" exact />
    </Router>
   </>
  );
}

export default App;
