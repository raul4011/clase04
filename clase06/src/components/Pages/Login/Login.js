import { useForm } from "react-hook-form"; //retorna 3 cosas este hook
import { Row, Col, Form, Button } from "react-bootstrap";
import { schema } from "./schema";
import { yupResolver } from "@hookform/resolvers/yup";
import {usePost} from "./../../../customHooks/useHTTP";



const Login = () => {
  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });

  const [post,data,fetching] = usePost();

  const submitForm = (data) => {
    console.log(data);
    console.log("users",data)
  };

  return (
    <>
      <Row className="justify-content-center">
        <Col md={6}>
          <Form onSubmit={handleSubmit(submitForm)}>
            <Form.Group>
              <Form.Control
                
                name="email"
                placeholder="Enter email"
                ref={register}
              ></Form.Control>
              {errors.email && (
                <span className="text-danger">{errors.email.message}</span>
              )}
            </Form.Group>
            <Form.Group>
              <Form.Control
                
                name="password"
                placeholder="********"
                ref={register}
              ></Form.Control>
               {errors.password && (
                <span className="text-danger">{errors.password.message}</span>
              )}
            </Form.Group>
            <Button type="submit">Ingresar</Button>
          </Form>
        </Col>
      </Row>
    </>
  );
};

export default Login;
