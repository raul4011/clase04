import * as yup from "yup";

export const schema = yup.object().shape({
    email: yup.string().email().required("este campo es obligatorio"),
    password: yup.string().required("este campo es obligatorio").min(3,"como minimo 3 caracteres").max(10,"como maximo 10 caracteres")
})