import {useReducer, useEffect} from "react"
import {initialState, productsReducer} from "./../../reducers/products"
import {FETCHING, FETCH_SUCCESS } from "./../../reducers/actions/products"
import PropTypes from "prop-types";
import axios from "axios";
import {BASE_URL} from "./../../Constants/index";

const Products = ({search}) => {
    const [state, dispatch] = useReducer(productsReducer, initialState);
    const getProducts = async () => {
        try {
            const {data : info} = await axios.get(`${BASE_URL}/sites/MLA/search?q=${search}&limit=8`)
            dispatch({type:FETCH_SUCCESS , payload :{data : info.results}});
        } catch (e) {
            console.error(e)
        }
    }

    useEffect(() => {
        getProducts();
    }, [search]);




    return ( 
        <>
            <h3>Productos a traer:{search}</h3>
        </>
     );
}

Products.propTypes = {
    search: PropTypes.string
}

export default Products;