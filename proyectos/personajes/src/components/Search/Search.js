import {useState} from "react"; 
import {Row,Col,Button,Form} from "react-bootstrap";


const Search = ({handlerSearch}) => {

    const [characters,setCharacters] = useState("");
    

    const handlerSubmit = (e) => {
        e.preventDefault();
        if (characters.trim() === "") return;
        handlerSearch(characters);
    }

    const handlerInput = (e) => {
        setCharacters(e.target.value);
    }

  return (
    <Row className="justify-content-center">
        <Col md={6}>
            <Form onSubmit={handlerSubmit} className="mt-4">
                <Form.Group>
                    <Form.Control 
                    placeholder="Buscar por nombre"
                    type="text"
                    onChange={handlerInput}
                    />
                </Form.Group>
                <Button type="submit" className="mt-4">Buscar</Button>
            </Form>       
        </Col>

    </Row>
  
  );
};

export default Search;
