import { Col, Card, Button } from "react-bootstrap";
import moment from "moment";


const Character = ({ id, name, gender, image,status , species , created }) => {
  return (
    <Col md={4} className="text-center mb-5 mt-5">
      <Card>
        <Card.Img variant={"top"} src={image} />
        <Card.Body>
          <Card.Title>
            {name}-{gender}
          </Card.Title>
        </Card.Body>
        <Card.Text>
          <p className={status === "Alive" ? "text-primary" : "text-danger"}>
              {status === "Alive" ? "VIVO" : "MUERTO"}-{species}</p>
          <small>{moment(created).format("DD/MM/yyyy") }</small>
        </Card.Text>
        <Button type="button" variant={status === "Alive" ? "success" : "danger"} block>Ver Mas</Button>
      </Card>
    </Col>
  );
};

export default Character;
