import {useEffect, useState} from "react";
import paginate from "./../Paginate";
import Character from "./character";
import {Row} from "react-bootstrap";
import Loading from "./../loading"
import { useFetch } from '../../customHooks/useFetch';
import Paginate from "./../Paginate";
import Search from "../Search";

/*componente de logica (peticiones http + map)*/ 


const BASE_ENDPOINT = "character";

const Characters = ({search}) => {
    const [url, setUrl] = useState(BASE_ENDPOINT);

    useEffect(()=>{
        const newUrl = !search ? BASE_ENDPOINT : `${BASE_ENDPOINT}?name=${search}`;
        setUrl(newUrl);
    },[search]);

    //sacamos data fetching y error del hook personalizado useFetch()    
    const [data, fetching,] = useFetch(url);
    const { info,results: characters} = data;

    const handlePages = (newUrl) => {
        setUrl(`${BASE_ENDPOINT}?${newUrl}`)
    }

    return ( 
        <>
         <Row>
            {
             fetching ? (<Loading/> ): (characters.map((character)=>(

                 <Character key={character.id} {...character}/>)
             ))  
            }
         </Row>
         <Paginate  {...info} handlePages={handlePages}/>
        </>
     );
}
 
export default Characters;