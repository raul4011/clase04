import {useState} from "react";
import Characters from "./components/characters";
import './App.css';
import {Container} from "react-bootstrap";
import Search from "./components/Search";


function App() {

  const [search,setSearch] = useState("");

  const handlerSearch = (name) => {
    setSearch(name);
  } 

  return (
   <>
    <Container>
      <Search handlerSearch={handlerSearch}/>
      <Characters search={search}/>
    </Container>
   </>
  );
}

export default App;
